import socket
import sys

HOST = '127.0.0.1'
PORT = 65432
CHUNK_SIZE = 65500


def connection_type(argument):
    if argument == "udp":
        return socket.SOCK_DGRAM
    else:
        return socket.SOCK_STREAM


def stream_data_tcp(connection):
    total_length = 0
    messages = 0
    data = connection.recv(CHUNK_SIZE)

    while data:
        total_length += len(data)
        messages += 1
        data = connection.recv(CHUNK_SIZE)

    return conn_type, messages, total_length


def stream_data_udp(connection):
    total_length = 0
    messages = 0
    (data, addr) = connection.recvfrom(CHUNK_SIZE)

    while data:
        total_length += len(data)
        messages += 1
        (data, addr) = connection.recvfrom(CHUNK_SIZE)
    connection.sendto(b'', addr)

    return conn_type, messages, total_length


def stop_and_wait_data_tcp(connection):
    total_length = 0
    messages = 0

    data = connection.recv(CHUNK_SIZE)

    while data:
        total_length += len(data)
        messages += 1
        connection.send(b'1')
        data = connection.recv(CHUNK_SIZE)

    return conn_type, messages, total_length


def stop_and_wait_data_udp(connection):
    total_length = 0
    messages = 0
    (data, addr) = connection.recvfrom(CHUNK_SIZE)

    while data:
        total_length += len(data)
        messages += 1
        connection.sendto(b'', addr)
        (data, addr) = connection.recvfrom(CHUNK_SIZE)

    return conn_type, messages, total_length


def print_data(data):
    print("Protocol: ", data[0], " Messages: ", data[1], " Bytes Read: ", data[2])


def tcp(sock):
    sock.listen(1)
    while True:
        conn, addr = s.accept()
        with conn:
            if sys.argv[2] == "stream":
                stats = stream_data_tcp(conn)
            else:
                stats = stop_and_wait_data_tcp(conn)
            print_data(stats)


def udp(sock):
    while True:
        if sys.argv[2] == "stream":
            stats = stream_data_udp(sock)
        else:
            stats = stop_and_wait_data_udp(sock)
        print_data(stats)


conn_type = connection_type(sys.argv[1])

with socket.socket(socket.AF_INET, conn_type) as s:
    s.bind((HOST, PORT))
    if sys.argv[1] == "udp":
        udp(s)
    else:
        tcp(s)
