import socket
import time
import sys
import os

# HOST = '127.0.0.1'
HOST = '192.168.0.122'
PORT = 65432
TEST_FOLDER = './test_data'
CHUNK_SIZE = 65500


def connection_type(argument):
    if argument == "udp":
        return socket.SOCK_DGRAM
    else:
        return socket.SOCK_STREAM


def get_files(base_folder):
    return [base_folder + '/' + x for x in os.listdir(base_folder)]


def bytes_from_file(filename, size=CHUNK_SIZE):
    with open(filename, 'rb') as f:
        while True:
            chunk = f.read(size)
            if chunk:
                yield chunk
            else:
                break


def stream_data_tcp(sock):
    total_length = 0
    messages = 0

    files = get_files(TEST_FOLDER)
    for f in files:
        for b in bytes_from_file(f):
            total_length += len(b)
            messages += 1
            sock.send(b)
    sock.send(b'')

    return time.time() - start_time, messages, total_length


def stream_data_udp(sock):
    total_length = 0
    messages = 0

    files = get_files(TEST_FOLDER)
    for f in files:
        for b in bytes_from_file(f):
            total_length += len(b)
            messages += 1
            sock.sendto(b, (HOST, PORT))
    sock.sendto(b'', (HOST, PORT))
    sock.recvfrom(CHUNK_SIZE)

    return time.time() - start_time, messages, total_length


def stop_and_wait_data_tcp(sock):
    total_length = 0
    messages = 0

    files = get_files(TEST_FOLDER)
    for f in files:
        for b in bytes_from_file(f):
            total_length += len(b)
            messages += 1
            sock.send(b)
            sock.recv(CHUNK_SIZE)
    sock.send(b'')

    return time.time() - start_time, messages, total_length


def stop_and_wait_data_udp(sock):
    total_length = 0
    messages = 0

    files = get_files(TEST_FOLDER)
    for f in files:
        for b in bytes_from_file(f):
            total_length += len(b)
            messages += 1
            sock.sendto(b, (HOST, PORT))
            sock.recvfrom(CHUNK_SIZE)
    sock.sendto(b'', (HOST, PORT))

    return time.time() - start_time, messages, total_length


def print_data(data):
    print(
        "Transmission Time: ",
        data[0],
        "seconds Number of messages: ",
        data[1],
        " Bytes Sent: ",
        data[2]
    )


def tcp(sock):
    sock.connect((HOST, PORT))
    if sys.argv[2] == "stream":
        data = stream_data_tcp(sock)
    else:
        data = stop_and_wait_data_tcp(sock)
    return data


def udp(sock):
    if sys.argv[2] == "stream":
        data = stream_data_udp(sock)
    else:
        data = stop_and_wait_data_udp(sock)
    return data


start_time = time.time()
conn_type = connection_type(sys.argv[1])

with socket.socket(socket.AF_INET, conn_type) as s:
    if sys.argv[1] == "udp":
        stats = udp(s)
    else:
        stats = tcp(s)
    print_data(stats)
